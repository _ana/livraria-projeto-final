## Configuração inicial do projeto

Caso em sua máquina não esteja configurado o sail, siga os passos:

### Configuração o alias para o sail no linux/wsl

- primeiro vá para o diretório raiz do sistema
- abra a edição do arquivo .bashrc
- adicione a linha de alias para o arquivo .bashrc
```
cd 
nano .bashrc
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
```

### Configurando o alias para o sail no macOS

- primeiro vá para o diretório raiz do sistema
- abra a edição do arquivo .zshrc
- adicione a linha de alias para o arquivo .zshrc
```
cd 
nano .zshrc
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
```

### Baixando as bibliotecas

- abra o terminal e entre na pasta:
- execute o comando:
```
docker run --rm -u "$(id -u):$(id -g)" -v $(pwd):/var/www/html -w /var/www/html laravelsail/php81-composer:latest composer install --ignore-platform-reqs
```

### Executando o projeto

- no terminal, certifique-se que está na pasta do projeto;
- execute o comando 
```
sail up -d
```
- após todos os conteiners subirem, execute o comando para atualização das dependências:
```
sail yarn install
```
- agora faça as migrações para que o projeto possa ter a base de dados:
```
sail artisan migrate
sail artisan db:seed
sail artisan storage:link
```
- finalmente, rode o comando para subir o modo dev:
```
sail yarn run dev
```

### Credenciais para teste:

- perfil gerente: 
```
login: gerente@mail.com.br
senha: 123
```

- perfil comprador: 
```
login: comprador@mail.com.br
senha: 123
```

- perfil cliente 1: 
```
login: cliente1@mail.com.br
senha: 123
```

- perfil cliente 2: 
```
login: cliente2@mail.com.br
senha: 123
```

- perfil cliente 3: 
```
login: cliente3@mail.com.br
senha: 123
```