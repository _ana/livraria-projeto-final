export default function Profile() {
    return (
        <svg width="48" height="48" viewBox="0 0 65 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0_71_548)">
                <path d="M32.5 56C45.7548 56 56.5 45.2548 56.5 32C56.5 18.7452 45.7548 8 32.5 8C19.2452 8 8.5 18.7452 8.5 32C8.5 45.2548 19.2452 56 32.5 56Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M32.5 40C38.0228 40 42.5 35.5228 42.5 30C42.5 24.4772 38.0228 20 32.5 20C26.9772 20 22.5 24.4772 22.5 30C22.5 35.5228 26.9772 40 32.5 40Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M16.4492 49.8427C17.954 46.8788 20.2501 44.3894 23.0831 42.6505C25.916 40.9117 29.1752 39.9912 32.4992 39.9912C35.8233 39.9912 39.0824 40.9117 41.9154 42.6505C44.7483 44.3894 47.0444 46.8788 48.5492 49.8427" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </g>
            <defs>
                <clipPath id="clip0_71_548">
                    <rect width="64" height="64" fill="white" transform="translate(0.5)"/>
                </clipPath>
            </defs>
        </svg>
    )
}
